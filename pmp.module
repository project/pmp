<?php

/**
 * @file
 * Provides "project" & "task" node types.
 */

/**
 * Implementation of hook_node_info().
 */
function pmp_node_info() {
    return array(
        'project' => array(
            'name' => t('Project'),
            'module' => 'project',
            'description' => t('Create a new project.'),
            'has_title' => TRUE,
            'title_label' => t('Project Name'),
            'has_body' => TRUE,
            'body_label' => t('Project Description'),
            'min_word_count' => 0,
            'locked' => TRUE,
        ),
        'task' => array(
            'name' => t('Task'),
            'module' => 'task',
            'description' => t('Create a new project task.'),
            'has_title' => TRUE,
            'title_label' => t('Task Name'),
            'has_body' => TRUE,
            'body_label' => t('Task Description'),
            'min_word_count' => 3,
            'locked' => TRUE,
        ),
    );
}

/**
 * Implementation of hook_menu().
 */
function pmp_menu($may_cache) {
    $items = array();
    
    // Do not cache this menu item
    if (!$may_cache) {
        $items[] = array(
            'path' => 'node/add/project',
            'title' => t('Project'),
            'access' => user_access('create project'),
        );
        $items[] = array(
            'path' => 'node/add/task',
            'title' => t('Task'),
            'access' => user_access('create task'),
        );
        $items[] = array(
            'path' => 'client/autocomplete',
            'title' => t('Profile category autocomplete'),
            'callback' => 'client_autocomplete',
            'access' => user_access('administer users'),
            'type' => MENU_CALLBACK
        );
    }
    return $items;
}

/**
 * Implementation of hook_perm().
 */
function pmp_perm() {
    return array(
        'accept assignments',
        'manage projects',
        'create project',
        'edit project',
        'edit own project',
        'create task',
        'edit task',
        'edit own task',
        'own projects'
    );
}

/**
 * Implementation of hook_access().
 */
function project_access($op, $node) {
    global $user;
    
    if ($op == 'create') {
        return (user_access('create project'));
    }
    
    if ($op == 'update' || $op == 'delete') {
        return (user_access('edit own project') && ($user->uid == $node->uid));
    }
}

function task_access($op, $node) {
    global $user;
    
    if ($op == 'create') {
        return (user_access('create task'));
    }
    
    if ($op == 'update' || $op == 'delete') {
        return (user_access('edit own task') && ($user->uid == $node->uid));
    }
}

/**
 * project_perm_user returns a fapi ready multi-select list of users with a given permission
 */
function project_perm_user($perm) {
    $result = db_query("SELECT rid FROM {permission} WHERE perm LIKE '%$perm%'");
    if ($result) {
        while($rid = db_fetch_array($result)) {
            $rid_arr[] = implode($rid);
        }
        $rid_str = implode(' ,', $rid_arr);
        $result = db_query("SELECT DISTINCT(u.uid), u.name FROM {users} u INNER JOIN {users_roles} r ON u.uid = r.uid WHERE r.rid IN ($rid_str) ORDER BY u.name"); // make our query with the newly created $rid_str
        while($user = db_fetch_array($result)) {
           $options[$user['uid']] = t($user['name']);
        }
    }
    return $options;
}

function client_autocomplete($string) {
    $matches = array();
    $result = db_query("SELECT rid FROM {permission} WHERE perm LIKE '%own projects%'");
    if ($result) {
        while($rid = db_fetch_array($result)) {
            $rid_arr[] = implode($rid);
        }
        $rid_str = implode(' ,', $rid_arr);
        $result = db_query_range("SELECT DISTINCT(u.uid), u.name FROM {users} u INNER JOIN {users_roles} r ON u.uid = r.uid WHERE r.rid IN ($rid_str) AND u.name LIKE LOWER('%s%%') ORDER BY u.name", $string, 0, 10); // make our query with the newly created $rid_str
        while ($data = db_fetch_object($result)) {
            $matches[$data->name .'[uid='. $data->uid .']'] = check_plain($data->name);
        }
    }
    print drupal_to_js($matches);
    exit();
}

/**
 * Implementation of hook_form().
 */
function project_form($node) {    
    $type = node_get_types('type', $node);
    $form = array();
    include_once(drupal_get_path('module', 'date_api') .'/date.inc'); // Include the API.
    
    $form['client'] = array(
        '#type' => 'textfield',
        '#title' => t('Client'),
        '#autocomplete_path' => 'client/autocomplete',
        '#required' => TRUE,
        '#weight' => -11,
        '#attributes' => array('style' => 'width:250px;'),
    );
    $form['client_value'] = array(
        '#type' => 'value',
        '#value' => '',
    );
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => check_plain($type->title_label),
        '#required' => TRUE,
        '#default_value' => $node->title,
        '#weight' => -10,
        '#attributes' => array('style' => 'width:250px;'),
    );
    $form['pm'] = array(
        '#type' => 'select',
        '#title' => t('Project Managers'),
        '#options' => project_perm_user('manage projects'),
        '#multiple' => TRUE,
        '#required' => TRUE,
        '#weight' => -9,
    );
    $params = array(
        'label' => 'Start Date',
        'weight' => -7,
        'granularity' => array('M', 'D', 'Y'),
        'jscalendar' => 1,
    );
    $form['start'] = date_text_input($params);
    $form['start_id'] = array(
        '#id' => 'start', // assign unique id to the input for the sake of the jscalendar drop down if present
    );
    $form['start'] = array_merge($form['start']['value'], $form['start_id']); // merge id into the array created by date_text_input
    $params = array(
        'label' => 'Due Date',
        'weight' => -6,
        'granularity' => array('M', 'D', 'Y'),
        'jscalendar' => 1,
    );
    $form['due'] = date_text_input($params);
    $form['due_id'] = array(
        '#id' => 'due', // assign unique id to the input for the sake of the jscalendar drop down if present
    );
    $form['due'] = array_merge($form['due']['value'], $form['due_id']); // merge id into the array created by date_text_input
    $form['priority'] = array(
        '#type' => 'select',
        '#title' => t('Priority'),
        '#required' => TRUE,
        '#default_value' => 'moderate',
        '#options' => array(
            'minimal' => 'minimal',
            'low' => 'low',
            'moderate' => 'moderate',
            'high' => 'high',
            'urgent' => 'urgent',
        ),
    );
    $form['status'] = array(
        '#type' => 'select',
        '#title' => t('Status'),
        '#required' => TRUE,
        '#default_value' => 'pending',
        '#options' => array(
            'pending' => 'pending',
            'active' => 'active',
            'awaiting client' => 'awaiting client',
            'awaiting in house' => 'awaiting in house',
            'pending QC' => 'pending QC',
            'finished' => 'finished',
        ),
    );
    $form['body_filter']['body'] = array(
        '#type' => 'textarea',
        '#title' => check_plain($type->body_label),
        '#required' => TRUE,
        '#default_value' => $node->body,
        '#rows' => 7
    );
    $form['body_filter']['filter'] = filter_form($node->format);
    return $form;
}

function task_form($node) {
    $type = node_get_types('type', $node);
    
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => check_plain($type->title_label),
        '#required' => TRUE,
        '#default_value' => $node->title,
        '#weight' => -10
    );
    $form['body_filter']['body'] = array(
        '#type' => 'textarea',
        '#title' => check_plain($type->body_label),
        '#required' => TRUE,
        '#default_value' => $node->body,
        '#rows' => 7
    );
    $form['body_filter']['filter'] = filter_form($node->format);
    return $form;
}

function project_validate($node, $form) {
    if (isset($node->client)) {
        $client = $node->client;
        $find = '[';
        $client_uid = substr($client, (strpos($client, $find)+5), -1);
        $cuser['name'] = substr($client, 0, strpos($client, $find));
        $cuser = user_load($cuser);
        if ($cuser->uid == $client_uid) {
            if (user_access('own projects', $cuser)) {
                form_set_value($form['client_value'], $cuser->uid);
            }
            else {
                form_set_error('client', 'The selected user does not have project ownership priviledges!');
            }
        }
        else {
            form_set_error('client', 'Client user name and user id are not equal!');
        }
    }
    if (isset($node->pm)) {
        //form_set_error('pm', $node->pm);
        print_r($node->pm);
    }
}

function project_insert($node) {
    db_query("INSERT INTO {task} (nid, vid, client, assigned_to, project, start, due, status, priority, completion) VALUES (%d, %d, %d, %d, '%s', %d, %d, '%s', '%s', %d)",
        $node->nid, $node->vid, $node->client_value, $node->assigned_value, $node->title, $node->start_value, $node->due_value, $node->status, $node->priority, $node->completion);
}